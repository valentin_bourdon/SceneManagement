# Scene Management Package
A simple example showing how to load/unload scenes with adressables.

## Requirements
* Unity 2020.3 or later.

## Dependencies
* ```"com.unity.addressables": "1.19.11"```
* ```"com.unity.textmeshpro": "3.0.1"```
* ```"com.bourdonvalentin.scriptable-object-workflow": "https://gitlab.com/valentin_bourdon/ScriptableObject_Workflow.git"```

## Installing the Package
* Install adressables package and create group settings in Window/Asset Management/Adressables/Groups.
* Then, add the name of the package (found in package.json) followed by the repository URL to your Unity project's manifest.json or using the package manager in Unity.

```json
{
  "dependencies": {
    "com.bourdonvalentin.scene-management": "https://gitlab.com/valentin_bourdon/SceneManagement.git",
}
```

## License

Initial work made by [UnityTechnologies](https://github.com/UnityTechnologies), provided under the Apache License 2.0, see [COPY](./COPY) file.
All the changes made are released under the MIT License, see [LICENSE](./LICENSE).
