using UnityEngine;
using UnityEngine.Events;
using ScriptableObjectWorkflow;

namespace SceneManagement.Events
{
	/// <summary>
	/// This class is a used for scene loading events.
	/// Takes an array of the scenes we want to load and a bool to specify if we want to show a loading screen.
	/// </summary>
	[CreateAssetMenu(menuName = "Events/Scene/Load Scene Event")]
	public class LoadSceneEventSO : DescriptionBaseSO
	{
		public UnityAction<SceneSO[], bool, bool> OnLoadingRequested;

		public void RaiseEvent(SceneSO[] locationToLoad, bool showLoadingScreen = false, bool fadeScreen = false)
		{
			if (OnLoadingRequested != null)
			{
				OnLoadingRequested.Invoke(locationToLoad, showLoadingScreen, fadeScreen);
			}
			else
			{
				Debug.LogWarning("A Scene loading was requested, but nobody picked it up. " +
					"Check why there is no SceneLoader already present, " +
					"and make sure it's listening on this Load Event channel.");
			}
		}
	}
}
