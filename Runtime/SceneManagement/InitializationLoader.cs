using System.Collections;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using SceneManagement.Events;

namespace SceneManagement
{
	public class InitializationLoader : MonoBehaviour
	{
		[SerializeField] private SceneSO _managersScene = default;
		[SerializeField] private SceneSO _menuToLoad = default;
		SceneSO[] _scenesToLoad;

		[Header("Broadcasting on")]
		[SerializeField] private AssetReference _menuLoadChannel = default;

		private void Start()
		{
			//Load the persistent managers scene
			_managersScene.sceneReference.LoadSceneAsync(LoadSceneMode.Additive, true).Completed += LoadEventChannel;

			_scenesToLoad = new SceneSO[1];
			_scenesToLoad[0] = _menuToLoad;
		}

		private void LoadEventChannel(AsyncOperationHandle<SceneInstance> obj)
		{
			_menuLoadChannel.LoadAssetAsync<LoadSceneEventSO>().Completed += LoadMainMenu;
		}

		private void LoadMainMenu(AsyncOperationHandle<LoadSceneEventSO> obj)
		{
			obj.Result.RaiseEvent(_scenesToLoad, true);

			SceneManager.UnloadSceneAsync(0); //Initialization is the only scene in BuildSettings, thus it has index 0
		}

	}
}
