using UnityEngine;
using SceneManagement.Events;

namespace SceneManagement
{
    public class LoadScene : MonoBehaviour
    {
        [Header("Load Scene Channel")]
        [SerializeField]
        private LoadSceneEventSO _onPressButton;

        [Header("Scenes SO")]
        [SerializeField]
        private SceneSO[] _sceneToLoad;

        [SerializeField]
        private bool _showLoadScreen;

        public void OnButtonPress()
        {
            _onPressButton.RaiseEvent(_sceneToLoad, _showLoadScreen);
        }
    }
}
