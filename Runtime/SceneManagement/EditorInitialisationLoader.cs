using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.AddressableAssets;
using System.Collections.Generic;
using ScriptableObjectWorkflow.Events;
using SceneManagement.Events;


namespace SceneManagement
{
    public class EditorInitialisationLoader : MonoBehaviour
    {
#if UNITY_EDITOR
    [SerializeField] private SceneSO _thisSceneSO = default;
    [SerializeField] private SceneSO _persistentManagersSO = default;
    [SerializeField] private List<SceneSO> _additionnalScenes = default;
    [SerializeField] private AssetReference _notifyStartupChannel = default;
    [SerializeField] private VoidEventChannelSO _onSceneReadyChannel = default;
    SceneSO[] _scenesToLoad;


    private bool isColdStart = false;
    private void Awake()
    {
        if (!SceneManager.GetSceneByName(_persistentManagersSO.sceneReference.editorAsset.name).isLoaded)
        {
            isColdStart = true;
        }

        _scenesToLoad = new SceneSO[1];
        _scenesToLoad[0] = _thisSceneSO;
    }

    private void Start()
    {
        if (isColdStart)
        {
            _persistentManagersSO.sceneReference.LoadSceneAsync(LoadSceneMode.Additive, true).Completed += LoadAdditionnalScene;
        }
    }

    private void LoadAdditionnalScene(AsyncOperationHandle<SceneInstance> obj)
    {
        for (int i = 0; i < _additionnalScenes.Count; i++)
        {
            _additionnalScenes[i].sceneReference.LoadSceneAsync(LoadSceneMode.Additive, true).Completed += LoadEventChannel;
        }
    }

    private void LoadEventChannel(AsyncOperationHandle<SceneInstance> obj)
    {
       

        _notifyStartupChannel.LoadAssetAsync<LoadSceneEventSO>().Completed += OnNotifyChannelLoaded;
    }

    private void OnNotifyChannelLoaded(AsyncOperationHandle<LoadSceneEventSO> obj)
    {
        if (_thisSceneSO != null)
        {
            obj.Result.RaiseEvent(_scenesToLoad);
        }
        else
        {
            //Raise a fake scene ready event, so the player is spawned
            _onSceneReadyChannel.RaiseEvent();
            //When this happens, the player won't be able to move between scenes because the SceneLoader has no conception of which scene we are in
        }
    }

#endif

    }
}
