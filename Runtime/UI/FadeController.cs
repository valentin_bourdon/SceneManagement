using UnityEngine;
using SceneManagement.Events;
using UnityEngine.UI;

namespace SceneManagement
{
	public class FadeController : MonoBehaviour
	{
		[SerializeField] private FadeChannelSO _fadeChannelSO;
		[SerializeField] private Image _fadeImage;

		private float _startTime = 0;
		private float _startOpacity = 1;
		private int _endOpacity = 1;
		private float _duration = 0;
		private bool _isFading = false;
		private float _opacity = 1;

		private void OnEnable()
		{
			_fadeChannelSO.OnEventRaised += InitiateFade;
		}

		private void OnDisable()
		{
			_fadeChannelSO.OnEventRaised -= InitiateFade;
		}


		void Update()
		{

			if (_isFading && _duration > 0)
			{
				_opacity = Mathf.Lerp(_startOpacity, _endOpacity, (Time.time - _startTime) / _duration);
				_fadeImage.color = new Color(_fadeImage.color.r, _fadeImage.color.g, _fadeImage.color.b, _opacity);
				_isFading = _opacity != _endOpacity;
			}
		}

		/// <summary>
		/// Controls the fade-in and fade-out.
		/// </summary>
		/// <param name="fadeIn">If false, the screen becomes black. If true, rectangle fades out and gameplay is visible.</param>
		/// <param name="duration">How long it takes to the image to fade in/out.</param>
		/// <param name="color">Target color for the image to reach. Disregarded when fading out.</param>
		private void InitiateFade(bool fadeIn, float duration, Color desiredColor)
		{
			_fadeImage.color = desiredColor;

			if(!fadeIn){
				_startOpacity = 0;
				_endOpacity = 1;
				_fadeImage.color = new Color(_fadeImage.color.r, _fadeImage.color.g, _fadeImage.color.b,0);
			}
			else{
				_startOpacity = 1;
				_endOpacity = 0;
				_fadeImage.color = new Color(_fadeImage.color.r, _fadeImage.color.g, _fadeImage.color.b, 1);
			}

			_duration = duration;
			_startTime = Time.time;

			_isFading = true;
		}
	}
}
