# Code Editor Package for Scene Management

## [0.1.2] - 2022-04-26

### [CHANGE]

- Remove Input System package dependency
- Remove Sample Example

## [0.1.1] - 2022-04-12

### [CHANGE]

Remove Input Reader

## [0.1.0] - 2022-02-01

### This is the first release of *Unity Package Scene Management*.

Using the newly created api to integrate Scene Management with Unity.
